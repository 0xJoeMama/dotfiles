#!/usr/bin/env sh

# Sync Script
# Used for either installation of reloading of the configuration!

set -e 

echo "==================================================================="
echo "|                     dotfiles from xJoeMama                      |"
echo "==================================================================="
echo "I'm not responsible for any unintended behavior! Use at your risk!!"
echo
echo

if [ "$1" = "clean" ]; then
    echo "Cleaning cache since user specified!"
    echo
    rm -rf "$PWD/cache"
fi

echo "Destroying PC using viruses!"

# XMonad Related Config
echo "Moving XMonad configuration: "

echo "-- Moving Config..."
cp "$PWD/xmonad/xmonad.hs" "$HOME/.xmonad/xmonad.hs"

echo "-- Moving scripts..."
mkdir -p "$HOME/.xmonad/scripts"
cp $PWD/sh/xmonad/* "$HOME/.xmonad/scripts"

echo "-- Recompiling XMonad..."
xmonad --recompile 
xmonad --restart

echo "Done!"
echo


echo "Zsh Configuration: "

echo "-- Checking for Oh My Zsh installation..."

if [ -z "$ZSH" ]; then
	echo "-- -- Could not find an Oh My Zsh installation. Proceeding with installation!"
	# TODO: Add zsh-syntax-highlighting and zsh-autosuggestions as custom installed plugins!
	sh "$PWD/sh/install_omz.sh"
fi

echo "-- Found installation at $ZSH."
echo "-- Checking for plugins..."

plugins=("zsh-autosuggestions" "zsh-syntax-highlighting")

for plugin in "${plugins[@]}"
do
    echo "-- -- Checking for $plugin..."
    if [ ! -d "$ZSH/custom/plugins/$plugin" ]; then
	echo "-- -- Installing $plugin..."
        case $plugin in 
		"zsh-autosuggestions")
			git clone https://github.com/zsh-users/zsh-autosuggestions "${ZSH_CUSTOM:-$HOME/.oh-my-zsh/custom}/plugins/zsh-autosuggestions"
			;;
		"zsh-syntax-highlighting")
			git clone https://github.com/zsh-users/zsh-syntax-highlighting.git "${ZSH_CUSTOM:-$HOME/.oh-my-zsh/custom}/plugins/zsh-syntax-highlighting"
			;;
	esac
    else
	echo "-- -- -- Found it!"
    fi
done

echo "-- All plugins exist! Proceeding..."

echo "-- Moving configuration file..."
cp "$PWD/zsh/.zshrc" "$HOME/.zshrc"

echo "Done!"
