#!/usr/bin/env sh

set -e


currLayout=$(setxkbmap -query | rg layout | rg -o --word-regexp "us|gr")

case $currLayout in
	"gr")
		setxkbmap us
		dunstify "Keyboard Layout is now set to: us"
		;;
	"us")
		setxkbmap gr
		dunstify "Keyboard Layout is now set to: gr"
		;;
esac

