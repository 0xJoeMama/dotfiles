#!/usr/bin/env sh

set -e

echo "Oh My Zsh Installation: "
echo "(Consider checking out the Github for more information here -> https://github.com/ohmyzsh/ohmyzsh)"

echo "-- Creating cache directory if it doesn't exist..."
mkdir -p "$PWD/cache"
# Yoinked from the Oh My Zsh Docs
# sh -c "$(curl -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"
curl -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh > "$PWD/cache/omz-install.sh"

sh "$PWD/cache/omz-install.sh"
echo "Done!"
